#!/bin/bash
# shellcheck source=/dev/null   
if [ -f graphics.sh ];then
	source graphics.sh
else
	echo -e "${RED}graphics.sh file cannot be found${WHITE}"
	exit 1;
fi

function text_menu_manage_selinux(){
	if [ "$1" == "manage" ];then
		echo -e "1) Show modules"
		echo -e "2) Change of mode[disabled|enforcing|premissive]"
		echo -e "3) Status Selinux"
		echo -e "4) Back to main menu"
	else
		echo -e "1.mode enforcing"
		echo -e "2.mode permissive"
		echo -e "3.mode disabled"
	fi
}

function manage_selinux(){
	local text_menu=( "Show modules" "change of mode[default|enforcing|premissive]" "Status Selinux" "Back to main menu") 
	PS3="Select options: "
	export local COLUMN=40
	select menu in  "${text_menu[@]}" 
		do
			case "$menu" in 
				"${text_menu[0]}")
					sudo semodule -l 
					;;
				"${text_menu[1]}")
					text_menu_manage_selinux "mode"
					read -r -p "Select mode: " mode
					if [ "$mode" -eq 1 ];then
						setenforce enforcing
					elif [ "$mode" -eq 2 ];then
						setenforce permissive
					elif [ "$mode" -eq 3 ];then
						setenforce disabled
					else
						echo -e "${RED}Invalid option: $REPLY ${WHITE}"
					fi
					;;
				"${text_menu[2]}")
					sestatus
					;;	
				"${text_menu[3]}")
					main_choose
					;;
				*)
					clear
					echo -e "${RED}Invalid option: $REPLY ${WHITE}"
					text_menu_manage_selinux "manage"
					;;
			esac
	done 
}

function verify_extra_packages_selinux(){
	echo -e "${BLUE}Checking for additional selinux packages...${WHITE}"
		local extra_packages=( "selinux-utils" )
		for package in "${extra_packages[@]}" 
		do
			if sudo dpkg-query -l "$package" &> "/dev/null"; then
				echo "${GREEN}The package $package is installed${WHITE}"
			else
				echo -e "${CYAN}Installing package: $package ${WHITE}"
				sudo apt install "$package" -y &> "/dev/null"
			fi	
	done
}

function check_selinux_install(){
	echo -e "${BLUE}Checking for selinux...${WHITE}"
	if 	sudo dpkg-query -l "selinux-basics" &> "/dev/null"; then
		echo "${GREEN}Selinux is installed${WHITE}" && verify_extra_packages_selinux
	else
		echo "${CYAN}Installing selinux...${WHITE}"
		sudo apt install "selinux-basics" -y &> "/dev/null" && verify_extra_packages_selinux
	fi
}

function main_manage_selinux(){
	check_selinux_install
	manage_selinux
}

