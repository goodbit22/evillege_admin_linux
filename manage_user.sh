#!/bin/bash
# shellcheck source=/dev/null 

if [ -f graphics.sh ];then
	source graphics.sh
else
	echo -e "${RED}graphics.sh file cannot be found${WHITE}"
  	exit 1;
fi

function text_menu_manage_user(){
	if [ "$1" == "modify_user" ]; then
		echo -e "1) Change shell"
		echo -e "2) Change group"
		echo -e "3) Change username"
		echo -e "4) Modify password"
	elif [ "$1" == "modify_password" ]; then
		echo -e "1) Change the warning about changing password"
		echo -e "2) Change the maximum number of days a password is valid for"
		echo -e "3) Change the minimum number of days between password changes"
		echo -e "4) Change password"
	elif [ "$1" == "selection" ]; then
		echo -e "1) All users have the same name"
		echo -e "2) Each user has a different name"			
	elif [ "$1" == "start" ]; then
		echo -e "1) Create a new user"
		echo -e "2) Edit user"
		echo -e "3) Create multiple users"
		echo -e "4) Modify password"
		echo -e "5) View logged in user statistics"
		echo -e "6) Back to main menu"
	elif [ "$1" == "shell" ]; then
		echo -e "1) /bin/bash"
		echo -e "2) /bin/sh"
		echo -e "3) /bin/csh"
		echo -e "4) /bin/tcsh"
		echo -e "5) /bin/fish"
		echo -e "6) /bin/ksh"
		echo -e "7) /bin/zsh"
	fi 
}

function create_new_user(){
		local user_new_name="$1"
		sudo useradd "$user_new_name"
		echo -e "Create user: $user_new_name"
		read -r -p "change account default settings[Y/N] " change
		if [ "$change" ==  "Y"  ] || [ "$change" == "y" ]; then 
			read  -r -p "Enter the new shell you want to set for the user " shell
			sudo usermod "$user_new_name" -s "$shell"
			read -r -p "Enter the new group you want to set for the user " groups
			sudo usermod "$user_new_name" -g "$groups"
			read -r -p "Enter the new directory you want to set for the user: " directory_user
			sudo usermod "$user_new_name" -d "$directory_user"
		fi
		sudo passwd "$user_new_name"
		read -r -p "change password default settings[Y/N] " change
		if [ "$change" ==  "Y"  ] || [ "$change" == "y" ]; then 
			read  -r -p "Enter the minimum number of days between password changes [default 0] " option2
			read  -r -p "Enter the maximum number of days a password is valid for [default 0] " option3
			read  -r -p "Specify how many days the password change warning will be displayed [default 0] " option1
			sudo passwd -n  "$option2" -x  "$option3" -w  "$option1"    "$user_new_name"
		fi
		read -r -p "Activate a password change the next time you log in[Y/N] " change
		if [ "$change" ==  "Y"  ] || [ "$change" == "y" ]; then 
			sudo passwd  -e "$user_new_name"
		fi
}

function modification_password(){
	local option=("Change the warning about changing password" "Change the maximum number of days a password is valid for" 
					"Change the minimum number of days between password changes" "Change password")
	PS3="What you want to choose: "
	local user="$1"
	while  true
	do
		select selection in "${option[0]}" "${option[1]}" "${option[2]}"
		do
			case $selection in 
				"${option[0]}") 
					read  -r -p	"Specify how many days the password change warning will be displayed [default 0] " days			
					sudo password -n  "$days" "$user"
					break
					;;
				"${option[1]}")
					read -r -p "Enter the maximum number of days a password is valid for [default 0] " days 
					sudo password -x "$days"  "$user"
					break
					;;
				"${option[2]}")
					read -r -p "Enter the minimum number of days between password changes [default 0] " days
					sudo password -w "$days" "$user"
					break
					;;
				"${option[3]}")
					sudo password "$user"	
					break
					;;
				*)
					echo -e "${RED}Invalid option: $REPLY ${WHITE}"
					text_menu_manage_user "modify_password"
					break
					;;
			esac
		done
	done	
}

function check_install_shell(){
	shell="$1"; user="$2"; shell_path="$3";	
	sudo dpkg-query -l "$shell" &> /dev/null
	local status="$?"
	if [ "$status" -eq 0 ];then
		sudo usermod "$user_new_name" -s "$shell_path" && echo -e "the specified shell has been set"
	else
		echo -e "the specified shell is not installed"
		read -r -p "Do you want install shell[y/n]: " answer
		answer=$(echo "$answer" | tr "[:upper:]" "[:lower:]")
		if [ "$answer"  == "y" ]||[ "$answer" == "yes" ];then
			echo -e "installing shell: $shell"; sudo apt install -y "$shell" &> /dev/null
			sudo usermod "$user_new_name" -s "$shell_path" && echo -e "the specified shell has been set"
		else
			echo -e "the specified shell has not been set "
		fi
	fi
}

function modification_old_user(){
	text_menu_manage_user "modify_user"
	read -r -p "What you want to choose: " selection
	case "$selection" in
			"1")
						local user_new_name="$1"
						local option=("/bin/bash" "/bin/sh" "/bin/csh"  "/bin/tcsh" "/bin/fish" "/bin/ksh" "/bin/zsh" )
						PS3="Enter the new shell you want to set for the user: " 
						select shell in "${option[@]}"; do 
							case "$shell"  in  
								"${option[0]}")
												echo -e "choose shell: ${option[0]}"
												check_install_shell "bash" "$user_new_name" "${option[0]}"
												break
												;;
								"${option[1]}")
												echo -e "choose shell: ${option[1]}"
												sudo usermod "$user_new_name" -s "${option[1]}"
												break
												;;
								"${option[2]}")
												echo -e "choose shell: ${option[2]}"
												sudo usermod "$user_new_name" -s "${option[2]}"
												break
												;;
								"${option[3]}")
												echo -e "choose shell: ${option[3]}"
												check_install_shell "tcsh" "$user_new_name" "${option[3]}"
												break
												;;
								"${option[4]}")
												echo -e "choose shell: ${option[4]}"
												check_install_shell "fish" "$user_new_name" "${option[4]}"
												break
												;;
								"${option[5]}")
												echo -e "choose shell: ${option[5]}"
												check_install_shell "ksh" "$user_new_name" "${option[5]}"
												break
												;;
								"${option[6]}")
												echo -e "choose shell: ${option[6]}"
												check_install_shell "zsh" "$user_new_name" "${option[6]}"
												break
												;;
								*)
												echo -e "${RED}Invalid option: $REPLY ${WHITE}"
												text_menu_manage_user "shell"
												;;
							esac
						done
						;;
			"2")
						user_new_name="$1"
						read -r -p "Enter the new group: " groups
						sudo usermod "$user_new_name" -g "$groups"
						;;
			"3")
						user_new_name="$1"
						read  -r -p  "Enter the new username: " user_new_name
						sudo usermod "$user_new_name" -l "$user_new_name"
						;;
			"4")
						modification_password "$user_new_name"
						;;
	esac
}

function create_many_users(){
	text_menu_manage_user user "selection"
	read  -r -p "Choose " choice 
	while [ "$choice" != 1 ] && [ "$choice" != 2 ]; do
		if [ "$choice" == 1 ]; then
				read -r -p "enter a generic user: " user
				read -r -p "How many users you want to create: " count
				for (( i=1;  i<=count; count++));do
					check=$(less /etc/passwd | sed 's/:/ /g' | awk '{print $1}' | grep -w "${user}${i}")
					if [ "$check" == "" ];then
						sudo useradd  "${user}${i}"
					else	
						echo -e "enter user exist"			
					fi
				done
		elif [ "$choice" == 2 ]; then
				read  -r -p "How many users you want to create:	" count
				for (( i=1;  i<=count; count++));do
					read -r -p "Enter username " user
					check=$(less /etc/passwd | sed 's/:/ /g' | awk '{print $1}' | grep -w "$user")
					if [ "$check" == "" ];then
						sudo useradd "$user"
					else
						echo -e "The user exist"			
					fi
			done
		else
				echo -e "${RED}Invalid option: $REPLY ${WHITE}"
				text_menu_manage_user user
				read  -r -p "Choose " choice 
		fi	
	done
}

function user_statistic(){
	local user count_process_all count_process_user
	user="$(whoami)" 
	count_process_all=$(ps aux | wc -l )
	count_process_user=$(ps -au "$user" | wc -l)
	echo "Logged user: $user"
	echo "Logged from: $(who  | awk '{ print $3}')"
	echo "Number of processes running by user: $count_process_user"
	echo "Percentage of user processes to all processes: $(( ("$count_process_user" * 100) / "$count_process_all"))%"
}

function manage_users(){
	clear
	export local COLUMNS=20
	local options=("Create a new user" "Edit user" "Create multiple users" "Modify password"
	"View logged in user statistics" "Back to main menu")
	select option in "${options[@]}"
	do	
		case "$option" in
			"${options[0]}")
					read  -r -p "Enter username: " username
					user=$(less /etc/passwd | sed 's/:/ /g' | awk '{print $1}' | grep -w "$username")
					if [ "$user" == "" ]; then
						create_new_user "$username"
					else
						echo "this user was found"				
					fi
						main_choose
						;;
			"${options[1]}")
					read  -r -p "Enter username: " username
					user=$(less /etc/passwd | sed 's/:/ /g' | awk '{print $1}' | grep -w "$username")
					if [ "$user" == "" ]; then
						echo "there is no such user"
					else
						echo "this user was found"				
						modification_old_user "$username"
					fi
						clear
						main_choose
						;;
			"${options[2]}")
						create_many_users
						main_choose
						;;
			"${options[3]}")
					read  -r -p "Enter username: " username
					user=$(less /etc/passwd | sed 's/:/ /g' | awk '{print $1}' | grep -w "$username")
					if [ "$user" == "" ]; then
						echo "there is no such user"
					else
						echo "this user was found"				
						modification_password "$username"
					fi
						clear
						main_choose
						;;
			"${options[4]}")
						clear
						user_statistic
						main_choose
						;;
			"${options[5]}")
						clear
						main_choose
						;;
			*)
						echo -e "${RED}Invalid option: $REPLY ${WHITE}"
						text_menu_manage_user "start"	
						read -r -p "Select options: " option
						;;
		esac
	done
}