#!/bin/bash 
# shellcheck source=/dev/null      

if [ -f graphics.sh ]; then                                                                                   
	source graphics.sh                                                                                          
else                                                                                                          
	echo -e "${RED}graphics.sh file cannot be found${WHITE}"
  	exit 1;                                                                                                     
fi    

text_menu_host_block(){
	if [ "$1" == "start" ]; then
		echo -e "1) Manage iptables"
		echo -e "2) Manage hosts.deny and hosts.allow files"
		echo -e "3) Back to main menu"
	elif [ "$1" == "host" ]; then
		echo -e "1) Allow"
		echo -e "2) Deny"
		echo -e "3) Display TCP Wrappers rules"
		echo -e "4) Back"
	elif [ "$1" == "iptables" ]; then
		echo -e "1) Add rule"
		echo -e "2) Delete rule"
		echo -e "3) Display rules"
		echo -e "4) Back"
	fi
}

iptables_add(){
	local chains=("INPUT" "OUTPUT" "FORWAND")
	local setting_chain=""
	local pol=""
	PS3="Select chain: "
	select chain in "${chains[@]}"
		do
			case "$chain" in
    		"${chains[0]}") setting_chain="${chains[0]}" break;;
    		"${chains[1]}") setting_chain="${chains[1]}" break;;
    		"${chains[2]}") setting_chain="${chains[2]}" break;;
   			 *) echo "You haven't chosen anything"
  		esac
		done
	local policy=("ACCEPT" "DROP" "REJECT")
	PS3="Select policy: "
	select select_policy in "${policy[@]}"
		do
			case "$select_policy" in
    		"${policy[0]}") pol="ACCEPT" break;;
    		"${policy[1]}") pol="DROP" break;;
    		"${policy[2]}") pol="REJECT" break;;
   			 *) echo "You haven't chosen anything"
  		esac
		done
	read -rp "Enter protocol[tcp,udp]: " ports
	read -rp "Enter the destination port number: " dport
	sudo iptables -A "$setting_chain" -p "$ports" --dport "$dport" -j "$pol" && iptables-save > /etc/iptables/rules.v4 && \
	echo "Add entry: $setting_chain -p $ports --dport $dport -j $pol"

}

iptables_delete(){
	local chains=("INPUT" "OUTPUT" "FORWAND")
	PS3="Select chain: "
	select chain in "${chains[@]}"
		do
			case "$chain" in
    		"${chains[0]}") setting_chain="${chains[0]}" break;;
    		"${chains[1]}") setting_chain="${chains[1]}" break;;
    		"${chains[2]}") setting_chain="${chains[2]}" break;;
   			 *) echo "You haven't chosen anything"
  		esac
		done
	read -rp "Enter rule number: " number
	sudo iptables -D "$setting_chain" "$number" && iptables-save > /etc/iptables/rules.v4 && \
	echo "Delete entry: $setting_chain $number"
}

iptables_display(){
	sudo iptables -L --line-numbers
}

iptable_manage(){
	local options=("Add rule" "Delete rule" "Display rules" "Back")                         
	PS3="${MAGENTA}Select options: ${WHITE}"
	select option in "${options[@]}"
	do
		case "$option" in
		"${options[0]}")
			iptables_add
			;;
		"${options[1]}")
			iptables_delete
			;;
		"${options[2]}")
			iptables_display
			;;
		"${options[3]}")
			clear
			main_block
			;;
		*)
			clear
			text_menu_host_block "iptables"
			;;
		esac
	done
}


allow_hosts(){
	read -rp "Enter service: " service
	read -rp "Do you allow everyone[y/n]: " question
	if [ "$question"  ==  "y" ];then
		ip="ALL" 	
	else
		read -rp "Enter address ipv4: " ip
	fi
	if 	echo "$service : $ip" >> /etc/hosts.allow; then
		echo "Add entry to /etc/hosts.allow file"	
	else
		echo "no entry has been added"
	fi
	unset st
}

deny_hosts(){
	read -rp "Enter service: " service
	read -rp "Do you deny everyone[y/n]: " question
	if [ "$question"  ==  "y" ];then
		ip="ALL" 	
	else
		read -rp "Enter address ipv4:  " ip
	fi
	echo "$service : $ip" >> /etc/hosts.deny
	local st="$?"
	if  [ "$st" -eq 0 ];then
		echo "Add entry to /etc/hosts.deny file"	
	else
		echo "no entry has been added"
	fi
	unset st
}

display_tcp_wrappers(){
	echo -e "Content of hosts.allow file"
	content_allow=$(grep -v ^'#' /etc/hosts.allow); echo "$content_allow"
	echo -e "\nContent of hosts.deny file"
	content_deny=$(grep -v ^'#' /etc/hosts.deny); echo "$content_deny"
}

hosts_manage(){
	export local COLUMNS=13  			
	local options=("Allow" "Deny" "Display TCP Wrappers rules" "Back")                         
	PS3="${MAGENTA}Select options: ${WHITE}"
	select option in "${options[@]}"
	do
		case "$option" in
		"${options[0]}")
			allow_hosts
			;;
		"${options[1]}")
			deny_hosts
			;;
		"${options[2]}")
			display_tcp_wrappers
			;;
		"${options[3]}")
			clear
			main_block
			;;
		*)
			clear
			text_menu_host_block "host"
			;;
		esac
	done
}

main_block(){
	export local COLUMNS=13  			
	local options=("Manage iptables" "Manage hosts.deny and hosts.allow files" "Back to main menu")                         
	PS3="${MAGENTA}Select options: ${WHITE}"
	select option in "${options[@]}"
	do
		case "$option" in  
		"${options[0]}")
			clear
			iptable_manage
			;;
		"${options[1]}")
			clear
			hosts_manage
			;;
		"${options[2]}")
			clear
			main_choose
			;;
		*)
			clear
			text_menu_host_block "start"
			;;
		esac
	done
	
}
