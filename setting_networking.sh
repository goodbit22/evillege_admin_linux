#!/bin/bash
# shellcheck source=/dev/null  

if [ -f graphics.sh ]; then
	source graphics.sh
else
	echo -e "${RED}graphics.sh file cannot be found${WHITE}"
	exit 1;
fi

function text_menu_setting_network(){
	echo "1) List network interface"
	echo "2) Configure gateway address" 
 	echo "3) Configure dns server address"
	echo "4) Configure network address"	
	echo "5) Back to main menu"
}

function list_interfaces(){
	interfaces_all=$(ls /sys/class/net)
	declare -i count=1
	echo -e "All network interfaces: "
	for interface in $interfaces_all
	do
			echo -e "${count}.${interface}"
			count=$(( count + 1 ))
	done
}

function setting_network(){
	PS3="Select options: "
	local options=("List network interface" "Configure gateway address" "Configure dns server address" "Configure network address" "Back to main menu")	
	select option in "${options[@]}"
	do				
		case "$option" in
			"${options[0]}")
					list_interfaces
					read -r -p "Enter the name of the network interface: " interfaces
					verification_interface "$interfaces"
					local verification=$?
					if [ "$verification" -eq "0" ];then
						echo "${GREEN}The network interface $interfaces was found${WHITE}"
						sudo ip address show "$interfaces"
						echo -e '\n'
						text_menu_setting_network
					else
						clear
						echo "${RED}The network interface $interfaces was not found${WHITE}"
						text_menu_setting_network
					fi
					;;
		
			"${options[1]}")
					list_interfaces
					read -r -p "Enter the name of the network interface: " interfaces
					verification_interface "$interfaces"
					local verification=$?
					if [ "$verification" -eq "0" ];then
						echo "${GREEN}The network interface $interfaces was found${WHITE}"
						read -r -p "Podaj adres gateway " gateway
						sudo ip route replace  "$gateway" dev "$interfaces"
						echo -e '\n'
						text_menu_setting_network
					else
						clear
						echo "${RED}The network interface $interfaces was not found${WHITE}"
						text_menu_setting_network
					fi
					;;
		
			"${options[2]}")
					list_interfaces
					read -r -p "Enter the name of the network interface: " interfaces
					verification_interface "$interfaces"
					local verification=$?
					if [ "$verification" -eq "0" ];then
						echo "${GREEN}The network interface $interfaces was found${WHITE}"
						read -r -p "Enter dns server name: " dns_name
						read -r -p "Enter dns server address: " dns_address
						echo "$dns_name" "$dns_address" >> /etc/resolv.conf
						echo -e '\n'
					else
						clear
						echo "${RED}The network interface $interfaces was not found${WHITE}"
						text_menu_setting_network
					fi
					;;

			"${options[3]}")
					list_interfaces
					read -r -p "Enter the name of the network interface: " interfaces
					verification_interface "$interfaces"
					local verification=$?
					if [ "$verification" -eq "0" ];then
						echo "${GREEN}The network interface $interfaces was found${WHITE}"
						local address, prefix
						address=$(ip addr show "$interfaces"  | grep inet | awk '{print $2}' | sed 's:/: : ' | awk '{print $1}')
						prefix=$(ip addr show "$interfaces" | grep inet | awk '{print $2}' | sed 's:/: : ' | awk '{print $2}')
						echo -e "Network interface: $interfaces \n  ipv4 address: $address  \n  subnet mask prefix: /$prefix  "
						sudo ip addr del "${address}/${prefix}" dev "$interfaces"
						read -r -p "Enter ipv4 address: " address_new
						read -r -p "Enter subnet mask prefix: " prefix_new
						sudo ip addr add "${address_new}/${prefix_new}" dev "$interfaces"
						unset address, prefix
						echo -e '\n'
						text_menu_setting_network
					else
						clear
						echo "${RED}The network interface $interfaces was not found${WHITE}"
						text_menu_setting_network
					fi
					;;
			"${options[4]}")
					clear 
					main_choose
					;;
			
			*)
					clear
					echo -e "${RED}Invalid option: $REPLY ${WHITE}"
					text_menu_setting_network
					;;
		esac
	done
}

function verification_interface(){
	interfaces_all=$(ls /sys/class/net)
	local interfaces="$1"
	for i in $interfaces_all
	do
		if [ "$i" == "$interfaces" ]; then
			return 0 
			break
		fi
	done
	return 1 
}
