#!/bin/bash
# shellcheck source=/dev/null 

if [ -f graphics.sh ];then
	source graphics.sh
else
	echo -e "${RED}graphics.sh file cannot be found${WHITE}"
	exit 1;
fi

function operations_pendrive(){
	local pendrive="$1"
	echo "$pendrive"
	sudo umount "$pendrive"
	echo "${GREEN}Unmounted flash drive${WHITE}"
	sudo mkfs.vfat "$pendrive"
	echo "${GREEN}Formated flash drive${WHITE}"
	while [[ "$iso" != *.iso ]]
	do
  	read -rp "Enter the path to the iso file: " path_iso
		iso="${path_iso##*/}"
		[[ "$iso" != *.iso ]] && echo "${YELLOW}Invalid iso file or there are no such iso file${WHITE}"

	done
	sudo dd if="$path_iso" of="$pendrive" bs=4M && sync
}

function one_system(){
	local REMOVABLE_DRIVES=""
	for _device in /sys/block/*/device; do
  	if readlink -f "$_device" | grep -E -q "usb"; then
     	disk=$(echo "$_device" | cut -f4 -d/)
		REMOVABLE_DRIVES="$disk"
    fi
	done
	if [ "$REMOVABLE_DRIVES" == ""  ]; then
			echo "${RED}no flash drive inserted${WHITE}"
	else
			echo "${GREEN}Flash drive detected${WHITE}"
			read -rp "Do you want to burn the iso image[y/n] " pyt
				if [ "$pyt" == "y" ]; then
					echo "Starting the process .. "; operations_pendrive "$REMOVABLE_DRIVES"
				else
					echo "Stopping the process"
				fi
			true
	fi
}

function text_menu_boot_pendrive(){
	echo -e "1) Boot single system"
	echo -e "2) Back to main menu"
}

function boot_pendrive(){
	local options=("Boot single system" "Back to main menu")
	PS3="Select options: "
	select option in "${options[@]}"
	do
		case "$option" in 
			"${options[0]}")
				one_system
				main_choose
				;;
			"${options[1]}")
				clear
				main_choose
				;;
			*)
				clear
				echo -e "${RED}Invalid option: $REPLY ${WHITE}"
				text_menu_boot_pendrive
		esac
	done
}
