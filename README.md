# Evilege_admin

![evile image](./img/graphics.png)

The Evilege_admin allows you to manage Linux and make it easy to use

![image](./img/menu.jpg)

## Table of Content

* [Features](#features)
* [Technologies](#technologies)
* [Running](#running)
* [Author](#author)

## Features

1) Backup
2) User management
3) Computer specs display
4) Network management
5) Creating a bootable pendrive
6) Creating bootable DVD/CD
7) Iptables rules management
8) Jobs to cron
9) Nmap scan
10) Securely delete files

## Technologies

* bash 5.0
* awk 5.0.1

## Running

```sh
 sudo bash  main.bash
```

or

```sh
 sudo ./main.bash
```

## Author

*****
__goodbit22__ --> "https://gitlab.com/users/goodbit22"
*****
