#!/bin/bash
# shellcheck source=/dev/null 

if [ -f graphics.sh ]; then
	source graphics.sh
else
	echo -e "${RED}graphics.sh file cannot be found${WHITE}"
	exit 1
fi

function detail_computer(){
	neofetch
	echo "System Information:"
	echo -e "${BLUE}hostname:${WHITE} $(hostname) "
	echo "Load Average: $(cat /proc/loadavg)"
	echo "Uptime: $(uptime)" >> "$output_file"
	echo -e "${BLUE}Operating System:${WHITE} $(lsb_release -d | awk '{print $2,  $3}') "
	echo -e "${BLUE}Kernel version:${WHITE} $(uname -r) "
	pr=$(cat < /proc/cpuinfo  | grep -w "model name" | head -n 1 | sed s/"model name"// | 
					sed s/./""/ | sed s/:/""/)
	echo -e "${BLUE}Processor:${WHITE} $pr "	
	mem=$(cat < /proc/meminfo | grep -w  MemTotal | awk '{size=$2; printf("%3.0f",size/1024/1024 "Gb")}')
	echo -e "${BLUE}RAM:${WHITE} ${mem}Gb"
	disk=$(lsblk -io KNAME,TYPE,SIZE,MODEL | grep -w "disk" | 
					awk ' { size=$3; name=$4;  printf("%s %3.0f",name,size)}')
	echo -e "${BLUE}Hard drive:${WHITE} ${disk}Gb"
	sr=$(lsblk -io KNAME,TYPE,SIZE,MODEL | grep -w rom | awk '{print $4}')
	echo -e "${BLUE}Drive:${WHITE} $sr"	
	text=$(sudo lspci | grep -E -i --color 'network|ethernet')
	card_network=$(sudo lspci | grep -E -i --color 'network|ethernet' | cut -c 8-${#text})
	echo -e  "${BLUE}Network card:${WHITE}\n${card_network} "
	card_sound=$(sudo lspci -v | grep -wi "audio device:"| cut -c 8-400)
	echo -e "${BLUE}Audio device:${WHITE} \n${card_sound}"
	card_avg=$( sudo lspci -P | grep -E -w 'VGA' | cut -c 8-400)
	card_display=$( sudo lspci -P | grep -E -w 'Display' | cut -c 14-400)
	echo -e "${BLUE}Graphics Card:${WHITE} \n${card_avg}\n${card_display} "
	echo -e "${CYAN}[Press any key: .......]${WHITE}"
	read -r -s -n 1 key 
	if [[ $key = "" ]]; then 
		clear
		main_choose
	else
		clear
		main_choose
	fi
}