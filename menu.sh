#!/bin/bash
# shellcheck source=/dev/null

if [ -f graphics.sh ];then
	source graphics.sh
else
	echo -e "${RED}graphics.sh file cannot be found${WHITE}"; exit 1
fi

function main_text(){
	echo -e "1)${BLUE} Manage users${WHITE}"
	echo -e "2)${BLUE} Check the program update${WHITE}"
	echo -e "3)${BLUE} Manage network${WHITE}"
	echo -e "4)${BLUE} Add a task to cron${WHITE}"
	echo -e "5)${BLUE} Check the specifications${WHITE}"
	echo -e "6)${BLUE} Selinux configurations${WHITE}"
	echo -e "7)${BLUE} Create a bootable flash drive${WHITE}"
	echo -e "8)${BLUE} Create a boot disk${WHITE}"
	echo -e "9)${BLUE} Scan nmap${WHITE}"
	echo -e "10)${BLUE} Safely remove a file from the operating system${WHITE}"
	echo -e "11)${BLUE} Create backup${WHITE}"
	echo -e "12)${BLUE} Block address ipv4${WHITE}" 
	echo -e "13)${BLUE} Exit${WHITE}"
}

function main_choose(){
		view_banner
    	view_author_center 
		export local COLUMNS=13
		PS3="${MAGENTA}Select options: ${WHITE}"
		local options=("${BLUE}Manage users${WHITE}" "${BLUE}Check the program update${WHITE}" 
		"${BLUE}Manage network${WHITE}" "${BLUE}Add a task to cron${WHITE}" "${BLUE}Check the specifications${WHITE}"
		"${BLUE}Selinux configurations${WHITE}" "${BLUE}Create a bootable flash drive${WHITE}" "${BLUE}Create a boot disk${WHITE}" 
		"${BLUE}Scan nmap${WHITE}" "${BLUE}Safely remove a file from the operating system${WHITE}" 
		"${BLUE}Create backup${WHITE}" "${BLUE}Block address ipv4${WHITE}" "${BLUE}Exit${WHITE}")
		select option  in  "${options[@]}"
		do
			case "$option" in
			"${options[0]}") 
						manage_users
						break
						;;
			"${options[1]}") 
						clear
						check_new_version
						break
						;;
			"${options[2]}")
						clear
						setting_network
						break
						;;
			"${options[3]}")
						clear
						configure_task_crone
						break
						;;
			"${options[4]}")
						clear
						detail_computer
						break
						;;
			"${options[5]}")
						clear
						main_manage_selinux
						break
						;;
			"${options[6]}")
						clear
						boot_pendrive
						break
						;;
			"${options[7]}")
						clear
						create_cd_boot_disc
						break
						;;
			"${options[8]}")
						clear
						scan_nmap
						break
						;;
			"${options[9]}")
						clear
						main_shred_delete
						break
						;;
			"${options[10]}")
						clear
						main_backup
						break
						;;
			"${options[11]}")
						clear
						main_block
						break
						;;
			"${options[12]}")
						exit 0
						;;
			*)
						clear
						view_banner
						export local COLUMNS=13
    					view_author_center
						echo -e "${RED}Invalid option: $REPLY ${WHITE}"
						main_text
			 esac	
		done
}
