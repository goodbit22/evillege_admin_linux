#!/bin/bash
# shellcheck source=/dev/null  

if [ -f graphics.sh ];then
	source graphics.sh
else
	echo -e "${RED}graphics.sh file cannot be found${WHITE}"
  	exit 1;
fi

function text_menu_backup(){
	if [ "$1" == "start"  ];then 
		echo -e "1) Backup"
		echo -e "2) Restore backup"
		echo -e "3) Back to main menu"
	elif [ "$1" == "make" ];then
		echo -e "1) Full copy"
	else
		echo -e "1) Restore a full backup"
	fi
}

function make_backup(){
	local options=("Full copy") 
	PS3="Select options: "
	select option in "${options[@]}"
	do
		case "$option" in 
			"${options[0]}")
				read -rp "Enter the path to directory " katalog 	
				if [ -f "$katalog"  ];then 
					TIMESTAMP=$(date +"%Y%m%d%H%M%S")
					tar -czvf "${HOME}/copy_${TIMESTAMP}.tar.gz" "${katalog}" && echo "Backup completed:"
				else 
					echo "there are no such directory"
				fi
				;;
			*)
				text_menu_backup "make"
		esac	
	done		

}

function restore_backup(){
	local options=("Restore a full backup")
	PS3="Select options: "
	select option in "${options[@]}"
	do
		case "$option" in 
			"${options[0]}")
				read -rp "Enter the backup to restore  " kopia 
				read -rp "Where you want to extract the files " path
				tar -xzvf "$kopia" -C "$path"
				;;
			*)
				text_menu_backup "restore"
		esac	
	done		
}

function main_backup(){
	local options=( "Backup" "Restore backup" "Back to main menu")
	PS3="Select options: "
	select option in "${options[@]}"
	do
		case "$option" in 
			"${options[0]}")
				make_backup
				;;
			"${options[1]}")
				restore_backup
				;;
			"${options[2]}")
				clear
				main_choose
				;;
			*)
				clear
				echo -e "${RED}Invalid option: $REPLY ${WHITE}"
				text_menu_backup "start"
		esac	
	done		
}
