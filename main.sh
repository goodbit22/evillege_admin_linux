#!/bin/bash 

# shellcheck source=/dev/null
readonly scripts=('graphics.sh' 'manage_user.sh' 'computer_details.sh' 'task_crone.sh' 'setting_networking.sh'
'shred_delete.sh' 'cd_boot.sh' 'pendrive_boot.sh' 'backup.sh' 'scan_nmap.sh' 'selinux_config.sh' 'host_block.sh' 'menu.sh')

for script in "${scripts[@]}"
do
	# shellcheck source=/dev/null
	if [ -f "${script}" ]; then
		source "${script}"
	else
		echo -e "There is no required ${script} file"; exit 1
	fi
done

function authorization(){
	if [ "$UID" -eq 0 ];then
		clear
	else
		echo -e "${RED}Permission denied${WHITE}" ; exit 1
	fi
}

function check_run_program(){
	local count_run_program
	count_run_program=$(pgrep  -f 'sudo bash main.sh|sudo ./main.sh' | wc -l)
	[ "$count_run_program" -gt 2 ] && echo -e "${YELLOW}The main.sh program has already been run${WHITE}" && exit 1
}

function parameters_script(){
	case $1 in
		"-v")
			echo -e v"Current version: ${CYAN}1.7${WHITE}" ; exit 1 
			;;
		"-h")
			echo -e "-v - display the current program version"; exit 1
			;;
	esac
}

function check_new_version(){
	#poprawic aktualizacje zeby bylo takze usuwanie	
	if ping -c 2 www.google.pl >> /dev/null; then
		local path_current, path_update
		path_current=$(pwd)	
		readonly path_update="/tmp/update_programs"
		echo -e "Checking the program version...  "
		file_programs=(banner backup.sh cd_boot.sh pendrive_boot.sh selinux_config.sh  main.sh 
					graphics.sh computer_details.sh manage_user.sh  scan_nmap.sh shred_delete.sh
					setting_networking.sh task_crone.sh host_block.sh menu.sh)
		mkdir "$path_update" 
		sha256sum  "${file_programs[@]}" > /tmp/update_programs/sum_programs.txt
		cd "$path_update" || return 0 
		git clone https://gitlab.com/goodbit22/evillege_admin_linux/ &> /dev/null 
		cd evillege_admin_linux/ || return 0  && 
						sha256sum  "${file_programs[@]}" > /tmp/update_programs/git_sum_programs.txt && cd ..
		update_files=$(diff -u sum_programs.txt git_sum_programs.txt | grep -E "^\+"   | awk '{print $2}' | sed '1d')
		if [ "$update_files" == " " ]; then
			echo -e "${GREEN}You have the current version of the program${WHITE}"
		else
			for update_file in $update_files
			do
				sudo mv -f /evillege_admin_linux/"$update_file" "$path_current"
			done
		fi	
		sudo rm -r /tmp/update_programs
	else
		echo -e "${RED}Unable to check for updates due to lack of internet${WHITE}"
	fi
}

function check_configure(){
	status=("${GREEN}[OK]" "${YELLOW}[WARNING]" "${RED}ERROR")
	sudo dpkg-query -l "selinux-basics" &> "/dev/null"; echo -e "${CYAN}Installed Selinux${WHITE} ${status["$?"]}"
	ping -c 2 www.google.pl >> /dev/null; echo -e "${CYAN}Access to the network${WHITE} ${status["$?"]}"
	sudo dpkg-query -l "fail2ban" &> /dev/null; echo -e "${CYAN}Installed failbane${WHITE} ${status["$?"]}"
	sudo dpkg-query -l "acl" >> /dev/null; echo -e "${CYAN}Installed acl and attr${WHITE} ${status["$?"]}${BLUE}"
}

function check_distribute_linux(){
	echo -e "Checking the Linux distribution"
	local linux_distribute=("Debian" "Ubuntu" "Kali")
	check_distribute=$(lsb_release -i | awk '{print $3}')
	local count=1
	for distribute  in "${linux_distribute[@]}"
	do
		if [ "$check_distribute" == "$distribute" ];then
				clear
				break
		elif [ "${#linux_distribute[*]}" -eq "$count" ]; then 
				echo -e "Your distribution is not on the list"; exit 1
		fi
		count=$(( count + 1  ))
	done
}

function main_program(){
	parameters_script "$1"
	authorization
	check_run_program
	check_distribute_linux
	check_configure
	main_choose
}
main_program "$1"
