#!/bin/bash
# shellcheck source=/dev/null 

if [ -f graphics.sh ]; then
	source graphics.sh
else
	echo -e "${RED}graphics.sh file cannot be found${WHITE}"
	exit 1;
fi

function text_scan_nmap(){
	if [ "$1" == "text_scan" ];then				
		echo -e "1) Scan TCP SYN"
		echo -e "2) Scan UDP"
		echo -e "3) Scan TCP Null"
		echo -e "4) Scan TCP ACK"
		echo -e "5) Scan TCP Maimon"
		echo -e "6) Scan TCP Window"
		echo -e "7) Scan TCP FIN"
		echo -e "8) Back to main menu"
	fi
}

function additional_options(){
	read -r -p "Do you want to select the port[y/n]" selection			
	selection=$(echo "$selection" | tr "[:upper:]" "[:lower:]")
	if [ "$selection" == "y" ]||[ "$selection" == "yes" ];then
		read -r -p "Enter port number: " number_port
		if [ "$number_port" -le  65535 ]&&[ "$number_port" -ge  0 ];then
			echo -e "Port number $number_port has been selected";	
		else
			echo -e "${YELLOW}The port number $number_port entered is invalid and the default port is set to ${MAGENTA}80${WHITE}"
			local number_port=80
			local nmap_port="-p $number_port"
		fi
	else
		local number_port=
		echo -e "${CYAN}You did not select this option${WHITE}"
	fi
	read -r -p "Do you want to enable detection of services and their versions? " selection			
	selection=$(echo "$selection" | tr "[:upper:]" "[:lower:]")
	if [ "$selection" == "y" ]||[ "$selection" == "yes" ];then
		echo -e "${CYAN}This option was selected${WHITE}"
		local nmap_option1="-sV"
	else
		local nmap_option1=
		echo -e "${CYAN}You did not select this option${WHITE}"
	fi
	read -r -p "Do you want to enable operating system detection[y/n] " selection			
	selection=$(echo "$selection" | tr "[:upper:]" "[:lower:]")
	if [ "$selection" == "y" ]||[ "$selection" == "yes" ];then
		echo -e "${CYAN}This option was selected${WHITE}"
		local nmap_option2="-O"
	else
		echo -e "${CYAN}You did not select this option${WHITE}"
		local nmap_option2=
	fi
	sudo nmap "$nmap_option1" "$nmap_option2"  "$1" "$nmap_port" "$2" 
}

function choice_scan_nmap(){
	options=("Scan TCP SYN" "Scan UDP" "Scan TCP Null" "Scan TCP ACK" "Scan TCP Maimon" "Scan TCP Window" "Scan TCP FIN" "Back to main menu")
	PS3="Select scan: "
	select option in "${options[@]}"
	do
		case "$option" in 
			"${options[0]}")
					read -r -p "Enter address ipv4: " address
					additional_options "-sS" "$address"
					;;
			"${options[1]}")
					read -r -p "Enter address ipv4: " address
					additional_options "-sU" "$address"						
					;;
			"${options[2]}")
					read -r -p "Enter address ipv4: " address
					additional_options "-sN" "$address"
					;;
			"${options[3]}")
					read -r -p "Enter address ipv4: " address
					additional_options "-sA" "$address"
					;;
			"${options[4]}")
					read -r -p "Enter address ipv4: " address
					additional_options "-sM" "$address"
					;;
			"${options[5]}")
					read -r -p "Enter address ipv4: " address
					additional_options "-sW" "$address"
					;;
			"${options[6]}")
					read -r -p "Enter address ipv4: " address
					additional_options "-sF" "$address"
					;;
			"${options[7]}")
					clear 
					main_choose
					;;
			*)
					clear
					echo -e "${RED}Invalid option: $REPLY ${WHITE}"
					text_scan_nmap "text_scan"
					;;
			esac
		done
}

function scan_nmap(){
	if sudo dpkg-query -l "nmap" &> /dev/null; then
		choice_scan_nmap
	else
		echo -e "${YELLOW}Nmap is not installed${WHITE}"	
		echo "${CYAN}Installing nmap...${WHITE}"
		sudo apt install nmap -y &> /dev/null && choice_scan_nmap
	fi
}
