#!/bin/bash
# shellcheck source=/dev/null  

if [ -f graphics.sh ];then
	source graphics.sh
else
	echo -e "${RED}graphics.sh file cannot be found${WHITE}"
  	exit 1;
fi

function check_device_and_dvd_disk(){
	local drive="$1"
	local cdinfo
    blkid "$drive" &> /dev/null
	local status="$?"
	cdinfo=$(setcd -i "$drive" | sed  '1d'| tr -d "[:space:]" )
	if [ "$cdinfo" == "Nodiscisinserted" ]; then
		return 2
	elif [  "$status" -eq 0 ]; then 
		return 3
	else
		return 4
	fi
}

check_devices_exist(){
	drives=$1
	for drive in "${drives[@]}"
	do
		if [ "$drive" == "" ];then
			echo "${RED}No drive connected to the computer${WHITE}"
			return 1
		else
			return 0
			break
		fi
	done
}

cd_record(){
	local dvd="$1"
	while [[ "$iso" != *.iso ]] 
	do
  	read -rp "Enter the path to the iso file: " path_iso
		iso="${path_iso##*/}"
		[[ "$iso" != *.iso ]] && echo "${YELLOW}Invalid iso file or there are no such iso file${WHITE}"
	done
	sudo dd if="$path_iso" of="$drive" bs=4096 conv=noerror
}

function create_cd_boot_disc(){
	dpkg -s "setcd" &>  "/dev/null" ||   sudo apt install -y setcd &> "/dev/null"
	mapfile -t drives < <(find /dev/sr*)
	check_devices_exist "${drives[@]}"	
	local status="$?"
	if [ "$status" -eq 1 ];then
		true
	else
		PS3="Select drive: "
		select dvd in "${drives[@]}"
		do
			if [ "$dvd" != "" ];then
				echo "Selected drive:  $dvd"
				break
			else
				echo "No drive selected"
			fi
		done
		check_device_and_dvd_disk "$dvd"
		status="$?"
		if [ "$status" -eq 3 ];then
			echo "The disc is not empty"
		elif [ "$status" -eq 2 ]; then
			echo "There is no disc inserted"
		else
			echo "The disc is empty"
			read -rp "Do you want to burn the iso image[y/n] " pyt
			if [ "$pyt" == "y" ]; then
				echo "Starting the process .. "
				cd_record "$dvd"
			else
				echo "Stopping the process"
			fi
		fi
	fi
	main_choose
}
