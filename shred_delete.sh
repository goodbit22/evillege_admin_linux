#!/bin/bash
# shellcheck source=/dev/null   

if [ -f graphics.sh ]; then
	source graphics.sh
else
	echo -e "${RED}graphics.sh file cannot be found${WHITE}"
	exit 1;
fi

function text_menu_shred_delete(){
	echo "1) Delete one file"
	echo "2) Delete directory with all its contents"
 	echo "3) Back to main menu"
}

function blocked_files(){
	local blocked_delete_file=("banner" "computer_details.sh" "task_crone.sh" "selinux_config.sh" "pendrive_boot.sh"
	"main.sh" "manage_user.sh" "graphics.sh" "scan_nmap.sh" "setting_networking.sh" "shred_delete.sh" "menu.sh" "host_block.sh")
	file="$1"
	for files in "${blocked_delete_file[@]}"
	do
		if [ "$files" == "$file" ];then
				return 1
				break
		fi
	done
}

function delete_one_file(){
	read -r -p "Type the name of the file to be deleted: " file
	if [ -f "$file" ];then
		  blocked_files "$file"
			local verification=$?
			if [ "$verification" -eq 0  ]; then
				echo -e "${BLUE}Deletion of $file file in progress ......${WHITE}"
				shred -n 10  -z  -u  "$file"  && echo -e "${GREEN} Removed $file file${WHITE}"
			else
				echo -e "${YELLOW}File $file cannnot be deleted${WHITE}"
			fi				
	else
		echo -e "${YELLOW}The $file file is not a file or the $file file does not exist${WHITE}"
	fi
}

function check_many_files(){
	dict=$1
    mapfile -t files < <(find "$dict"  -type f  -name '*.*')
	mapfile -t directories  < <(find "$dict"  -type d | sort -nr) 	
	for file in "${files[@]}"
	do
		if [ -f "$file" ];then
			echo -e "${BLUE}Deletion of $file file in progress ......${WHITE}"
			shred -v -n 10 -z -u "$file" && echo -e "${GREEN} Removed $file file${WHITE}"
		fi
	done
	for directory in "${directories[@]}"
	do
		if [ ! -f "$directory" ];then
			rmdir  "$directory" && echo -e "${GREEN} Removed $directory directory${WHITE}"
		fi
	done

}

function delete_many_files(){
	read -r -p "Type the name of the directory which contents will be deleted: " directory
	if [ -d "$directory" ];then
		check_many_files "$directory"		
	else
		echo -e "${YELLOW}The $directory directory is not a directory or the $directory directory does not exist${WHITE}"
	fi
}

function  main_shred_delete(){
	PS3="Select options: "
	option=("Delete one file" "Delete directory with all its contents" "Back to main menu")
	select selection in "${option[@]}" 
	do
			case $selection in 
				"${option[0]}")
					delete_one_file
					text_menu_shred_delete
					;;
				"${option[1]}")
					delete_many_files
					text_menu_shred_delete
					;;
				"${option[2]}")
					clear
					main_choose
					;;
				*)
					clear
					echo -e "${RED}Invalid option: $REPLY ${WHITE}"
					text_menu_shred_delete
					;;
			esac
	done
}

