#!/bin/bash
# shellcheck source=/dev/null   

if [ -f graphics.sh ];then
	source graphics.sh
else
	echo -e "${RED}graphics.sh file cannot be found${WHITE}"
	exit 1;
fi

function check_user_command(){
	if [ "$2" == "check_user" ];then 
		local username="$1"
		user=$(less /etc/passwd | sed 's/:/ /g' | awk '{print $1}' | grep -w "$username")
		if [ "$user" == "" ]; then
			return 1;
		else
			return 0;			
		fi
	else
		local commands="$1"
		"$commands" &> /dev/null
		local status="$?"
		if [ "$status" -eq 0 ];then
			return 0;
		else
			return 1;			
		fi
	fi
}

function configure_task_crone(){
	read -r -p "Enter command: " commands
	check_user_command "$commands" "check_command"
	local status="$?"
	if [ ! "$status" -eq 0 ];then
		echo -e "${RED}command not found: $commands ${WHITE}"
		main_choose
	fi
	read -r -p "Enter user: " username
	check_user_command "$username" "check_user"
	local status="$?"
	if [ "$status" -eq 1 ];then
			echo -e "${YELLOW}user $username does not exist${WHITE}"				
			username=$(who | awk '{print $1}') && echo -e "the current user $username has been assigned"
	fi 				
	read -r -p "Minute[0-59]: " minute
	if [ "$minute" -lt 0   ]&&[  "$minute" -gt 59 ];then
			echo -e "${YELLOW}default values 0 have been assigned ${WHITE}"
			minute='0'	
	fi
	read -r -p "Hour[0-23]: " hour
	if [ "$hour" -lt 0 ]&&[ "$hour" -gt 23 ];then
			echo -e "${YELLOW}default values 0 have been assigned ${WHITE}"
			hour='0'	
	fi
	read -r -p "Day[1-31] " day
	if [ "$day" -lt 1 ]&&[ "$day" -gt 31 ];then
			echo -e "${YELLOW}default values 0 have been assigned ${WHITE}"
			day='0'	
	fi
	read -r -p "Month[1-12]: " month
	if [ "$month" -lt 1 ]&&[ "$month" -gt 12 ];then
			echo -e "${YELLOW}default values 0 have been assigned ${WHITE}"
			month='0'	
	fi
	read -r -p "Day of the Week[0-6]: " day_week
	if [ "$day_week" -lt 0 ]&&[ "$day_week" -gt 6 ];then
			echo -e "${YELLOW}default values 0 have been assigned ${WHITE}"
			day_week='0'	
	fi
	all="${minute} ${hour} ${day} ${month} ${day_week} ${username} ${commands}"
	"$all" >> /etc/crontab
	main_choose
}
