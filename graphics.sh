#!/bin/bash

function view_author_center(){
	tekst="//=====================================//"
	printf "%*s\n" $(((${#tekst}+COLUMNS)/2)) "${GREEN}$tekst"
	tekst="|| 	   ${RED}author:goodbit22${GREEN} 	       ||"
	printf "%*s\n" $(((${#tekst}-6+COLUMNS)/2)) "$tekst"
	tekst="|]=====================================[|${GREEN}"
	printf "%*s\n" $(((${#tekst}-4 +COLUMNS)/2)) "$tekst"
	tekst="||${BLUE} gitlab:https://gitlab.com/goodbit22${GREEN} ||"
	printf "%*s\n" $(((${#tekst} +COLUMNS)/2)) "$tekst"
	tekst="//=====================================//"
	printf "%*s\n" $(((${#tekst} +COLUMNS)/2)) "$tekst${WHITE}"	
	unset COLUMNS
}

function view_author(){
	echo -e "${color["green"]}//=====================================//"
	echo -e "${color["green"]}||${color["red"]} 	  author:goodbit22${color["green"]} 	       ||" 
	echo -e "|]=====================================[|"
	echo -e "||${color["blue"]} gitlab:https://gitlab.com/goodbit22${color["green"]} ||"
	echo -e "//=====================================//${color["white"]}"
	#unset COLUMNS
}

function view_banner(){
	banner1=$(cat  banner)
	echo -e "${color["blue"]}$banner1 ${color["white"]}"
}

MAGENTA=$(tput setaf 5)
RED=$(tput setaf 1) 
YELLOW=$(tput setaf 3)
GREEN=$(tput setaf 2) 
BLUE=$(tput setaf 4)
WHITE=$(tput setaf 7)
CYAN=$(tput setaf 6)
COLUMNS=$(tput cols)

declare -A color 
color=(["green"]="\e[32m" ["red"]='\e[31m' ["white"]="\e[37m"
["black"]="\e[37m" ["blue"]="\e[34m" ["effect"]="\e[5m"
["delete_effect"]="\e[25m")

